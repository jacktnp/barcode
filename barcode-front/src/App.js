import React from 'react';
// import './App.css';
import './style.css';
import AddProduct from './pages/AddProduct'
import ShowProduct from './pages/ShowProduct'
import updateProduct from './pages/updateProduct'
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import { Route } from 'react-router-dom'

function App(props) {
  // var Barcode = require('react-barcode');
  return (
    <div className="App">
      <Navbar>
        <Navbar.Brand href="/" id="NavbarFont">คลังสินค้า</Navbar.Brand>
        <Navbar.Toggle />
        <Navbar.Collapse className="justify-content-end">
          <Navbar.Text>
            <Nav.Link href="/show" id="NavbarFont">รายการสินค้า</Nav.Link>
          </Navbar.Text>
          <Navbar.Text>
            <Nav.Link href="/add" id="NavbarFont">เพิ่มสินค้า</Nav.Link>
          </Navbar.Text>
        </Navbar.Collapse>
      </Navbar>
      <div className="container my-5">
        <Route path="/show" component={ShowProduct} />
        <Route path="/add" component={AddProduct} />
        <Route path="/edit/:id" component={updateProduct} />
      </div>
      <footer>©2020 Barcode | WebApplication</footer>
    </div>
  );
}

export default App;
