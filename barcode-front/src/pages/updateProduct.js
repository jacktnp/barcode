import React, { Component } from "react";
import axios from "axios";
import "bootstrap/dist/css/bootstrap.css";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";

class updateProduct extends Component {
    state = {
      items: [],
      loading: true,
    }

    handleChange = name => e => {
        this.setState({ items: [{...this.state.items, [name]:e.target.value}]});
    };
    
  handleSubmit = (id) => (e) => {
    e.preventDefault();
    axios.patch(`http://localhost:3000/code/${id}`, this.state.items[0])
      .then(response => {
        console.log(response);
        alert("Success")
      })
      .catch(error => {
        console.log(error);
      });
  };

  handleGet = (id) => {
    axios.get(`http://localhost:3000/code/${id}`)
    .then((data) => {
      this.setState({ items: data.data, loading : false })
    })
  }
  componentDidMount() {
    this.handleGet(this.props.match.params.id)
  }
  
  render() {
    console.log(this.state.items)
    // let dataid = this.props.match.params.id;
    if(this.state.loading){
      return(<div></div>)
    }
    return (
      <div>
        <h3 className="my-3">แก้ไขสินค้า</h3>
        <Form method="POST" onSubmit={(e) => this.handleSubmit(this.props.match.params.id)(e)}>
          <Form.Group controlId="item_name">
            <Form.Label>ชื่อสินค้า</Form.Label>
            <Form.Control
              type="text"
              name="item_name"
              value={this.state.items[0].item_name}
              onChange={this.handleChange("item_name")}
            />
          </Form.Group>
          <Form.Group controlId="item_attribute1">
            <Form.Label>ไซส์เสื้อ</Form.Label>
            <Form.Control as="select" name="item_attribute1" value={this.state.items[0].item_attribute1}
              onChange={(e) => this.handleChange(e)}>
              <option value="XS">XS</option>
              <option value="S">S</option>
              <option value="M">M</option>
              <option value="L">L</option>
              <option value="XL">XL</option>
              <option value="2XL">2XL</option>
              <option value="3XL">3XL</option>
              <option value="4XL">4XL</option>
              <option value="5XL">5XL</option>
            </Form.Control>
          </Form.Group>
          <Form.Group controlId="item_attribute2">
            <Form.Label>สีเสื้อ</Form.Label>
            <Form.Control as="select" name="item_attribute2" value={this.state.items[0].item_attribute2}
              onChange={(e) => this.handleChange(e)}>
              <option value="Blue">Blue</option>
              <option value="Black">Black</option>
              <option value="Pink">Pink</option>
              <option value="Blue">Blue</option>
              <option value="Red">Red</option>
              <option value="Yellow">Yellow</option>
              <option value="Orange">Orange</option>
              <option value="Green">Green</option>
              <option value="Purple">Purple</option>
            </Form.Control>
          </Form.Group>
          <Form.Group controlId="item_type">
            <Form.Label>ประเภทสินค้า</Form.Label>
            <Form.Control
              type="text"
              name="item_type"
              value={this.state.items[0].item_type}
              onChange={(e) => this.handleChange(e)}
            />
          </Form.Group>
          <Form.Group controlId="number">
            <Form.Label>สินค้าคงคลัง</Form.Label>
            <Form.Control
              type="text"
              name="number"
              value={this.state.items[0].number}
              onChange={(e) => this.handleChange(e)}
            />
          </Form.Group>
          <Form.Group controlId="barcode_code">
            <Form.Label>
              Barcode code <small>(เลข 6 หลัก)</small>
            </Form.Label>
            <Form.Control
              type="text"
              placeholder="เลข 6 หลัก"
              value={this.state.items[0].barcode_code}
              name="barcode_code"
              onChange={(e) => this.handleChange(e)}
            />
          </Form.Group>
          <Form.Group controlId="barcode_company">
            <Form.Label>
              Barcode Company <small>(เลข 5 หลัก)</small>
            </Form.Label>
            <Form.Control
              type="text"
              placeholder="เลข 5 หลัก"
              value={this.state.items[0].barcode_company}
              name="barcode_company"
              onChange={(e) => this.handleChange(e)}
            />
          </Form.Group>
          <Button variant="warning w-100" type="submit">
            อัพเดตสินค้า
          </Button>
          </Form>
      </div>
    );
  }
}

export default updateProduct;
