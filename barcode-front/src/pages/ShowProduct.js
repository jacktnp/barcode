import React, { Component } from "react";
import axios from "axios";
import Table from "react-bootstrap/Table";
import Barcode from "react-barcode";
import { Link } from 'react-router-dom';

class ShowProduct extends Component{
  state = {
    items: []
  }

  /* DELETE */
  handleGet = (id) => {
    if(id == null){id = 'barcode'}
    axios.get(`http://localhost:3000/code/sort/${id}`).then(response => response.data)
    .then((data) => {
      this.setState({ items: data })
    })
  }
  componentDidMount() {
    this.handleGet()
  }

  handleDelete = id => {

    axios.delete(`http://localhost:3000/code/${id}`)
      .then(res => {
        alert("Deleted !")
        console.log(res);
        console.log(res.data);
        this.handleGet();
      })
  }

  render(){
    return (
        <div>
        <h3 className="my-3">รายการสินค้า</h3>
        <div className="text-left my-3">
            <span className="btn btn-primary btn-sm mr-1"><b>sort by :</b></span>
            <button className="btn btn-info btn-sm mr-1" onClick={() => this.handleGet('barcode')}>barcode</button>
            <button className="btn btn-info btn-sm mr-1" onClick={() => this.handleGet('date')}>date</button>
            <button className="btn btn-info btn-sm mr-1" onClick={() => this.handleGet('number')}>number</button>
            <button className="btn btn-info btn-sm mr-1" onClick={() => this.handleGet('company')}>company</button>
            <button className="btn btn-info btn-sm mr-1" onClick={() => this.handleGet('sku')}>sku</button>
            <button className="btn btn-info btn-sm mr-1" onClick={() => this.handleGet('item_type')}>item_type</button>
        </div>
        <Table striped bordered hover className="text-center">
            <thead>
            <tr>
                <th style={{ width: "5%" }}>#</th>
                <th style={{ width: "30%" }}>ชื่อสินค้า</th>
                <th style={{ width: "15%" }}>ประเภทสินค้า</th>
                <th style={{ width: "15%" }}>สินค้าคงคลัง</th>
                <th style={{ width: "20%" }}>รหัสสินค้า</th>
                <th style={{ width: "15%" }}>แก้ไข</th>
            </tr>
            </thead>
            <tbody>
            {this.state.items.map((data, index) => (
                <tr>
                <td>{index + 1}</td>
                <td>{data.item_name}</td>
                <td>{data.item_type}</td>
                <td>{data.number}</td>
                <td>
                    {data.sku}
                    <br></br>
                    <Barcode value={data.barcode_code + data.barcode_company} />
                </td>
                <td>
                    <Link className="btn btn-info btn-sm mr-1" to={`/edit/${data.sku}`}>แก้ไข</Link>
                    <button className="btn btn-danger btn-sm" onClick={() => this.handleDelete(data.sku)}>ลบสินค้า</button>
                </td>
                </tr> 
            ))}
            </tbody>
        </Table>
        </div>
    );
  }
}

export default ShowProduct;