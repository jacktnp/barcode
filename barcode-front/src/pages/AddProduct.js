import React, { Component } from "react";
import axios from "axios";
import "bootstrap/dist/css/bootstrap.css";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import { useAlert } from "react-alert";

class AddProduct extends Component {
  constructor() {
    super();

    var dateObj = new Date();
    var month = dateObj.getUTCMonth() + 1; //months from 1-12
    var day = dateObj.getUTCDate();
    var year = dateObj.getUTCFullYear();
 
    this.state = {
      item_name: '',
      item_attribute1: '',
      item_attribute2: '',
      item_type: '',
      item_code: '',
      number: '',
      barcode_code: '',
      barcode_company: '',
      date: year + "-" + month + "-" + day
    };
  }
  
  handleChange = (e) => {
      this.setState({ [e.target.name]: e.target.value });
  };

  handleSubmit = ({res, fields, form}) => {
    axios.post("http://localhost:3000/code/", this.state)
      .then(response => {
        console.log(response);
        // form.reset()
        alert("Success")
      })
      .catch(error => {
        console.log(error);
      });
  };

  render() {

    const {
      item_name,
      item_attribute1,
      item_attribute2,
      item_type,
      item_code,
      number,
      barcode_code,
      barcode_company
    } = this.state;

    return (
      <div>
        <h3 className="my-3">เพิ่มสินค้า</h3>
        <Form onSubmit={this.handleSubmit}>
          <Form.Group controlId="item_name">
            <Form.Label>ชื่อสินค้า</Form.Label>
            <Form.Control
              type="text"
              name="item_name"
              value={item_name}
              onChange={this.handleChange}
              required
            />
          </Form.Group>
          <Form.Group controlId="item_attribute1">
            <Form.Label>ไซส์เสื้อ</Form.Label>
            <Form.Control as="select" name="item_attribute1" value={item_attribute1} onChange={this.handleChange}>
              <option value="XS">XS</option>
              <option value="S">S</option>
              <option value="M">M</option>
              <option value="L">L</option>
              <option value="XL">XL</option>
              <option value="2XL">2XL</option>
              <option value="3XL">3XL</option>
              <option value="4XL">4XL</option>
              <option value="5XL">5XL</option>
            </Form.Control>
          </Form.Group>
          <Form.Group controlId="item_attribute2">
            <Form.Label>สีเสื้อ</Form.Label>
            <Form.Control as="select" name="item_attribute2" value={item_attribute2} onChange={this.handleChange}>
              <option value="Blue">Blue</option>
              <option value="Black">Black</option>
              <option value="Pink">Pink</option>
              <option value="Blue">Blue</option>
              <option value="Red">Red</option>
              <option value="Yellow">Yellow</option>
              <option value="Orange">Orange</option>
              <option value="Green">Green</option>
              <option value="Purple">Purple</option>
            </Form.Control>
          </Form.Group>
          <Form.Group controlId="item_type">
            <Form.Label>ประเภทสินค้า</Form.Label>
            <Form.Control
              type="text"
              name="item_type"
              value={item_type}
              onChange={this.handleChange}
              required
            />
          </Form.Group>
          <Form.Group controlId="number">
            <Form.Label>สินค้าคงคลัง</Form.Label>
            <Form.Control
              type="text"
              name="number"
              value={number}
              onChange={this.handleChange}
              required
            />
          </Form.Group>
          <Form.Group controlId="barcode_code">
            <Form.Label>
              Barcode code <small>(เลข 6 หลัก)</small>
            </Form.Label>
            <Form.Control
              type="text"
              placeholder="เลข 6 หลัก"
              value={barcode_code}
              name="barcode_code"
              onChange={this.handleChange}
              required
            />
          </Form.Group>
          <Form.Group controlId="barcode_company">
            <Form.Label>
              Barcode Company <small>(เลข 5 หลัก)</small>
            </Form.Label>
            <Form.Control
              type="text"
              placeholder="เลข 5 หลัก"
              value={barcode_company}
              name="barcode_company"
              onChange={this.handleChange}
              required
            />
          </Form.Group>
          <Button variant="outline-primary w-100" type="submit">
            เพิ่มสินค้า
          </Button>
        </Form>
      </div>
    );
  }
}

export default AddProduct;
