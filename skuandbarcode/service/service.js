const express = require('express');
const mongoose = require('mongoose');
const item = require('../db/Queue');
const abbreviate = require('abbreviate')
const route = express.Router();



//  เอาไว้ post ค่าใส่เข้า db 
route.post('/', async (req, res) => {
    const {
        item_name, item_attribute1, item_type,
        item_attribute2, item_code, number, date, 
        // sku_name_numder, sku_code_numder, sku_attr1_numder, sku_attr2_numder,
        barcode_code, barcode_company
    } = req.body;
    const sku_insert = {
        sku: makeSku(item_name,item_attribute1,item_type,item_attribute2)
    }
    console.log("skusad: "+sku_insert.sku)

    const newappointment = await Item.find(sku_insert)
    console.log("ered"+newappointment)
    if (newappointment.length == 0) {
        try {
            if (barcode_code.length != 6 || typeof barcode_code != "string") {
                throw new Error('barcode_company 6 String error')

            }
            if (barcode_company.length != 5 || typeof barcode_company != "string") {
                console.log('barcode_company  String error')
                throw new Error('barcode_company 5 String error')

            }
            let item = {};
            item.item_name = item_name;
            item.item_attribute1 = item_attribute1;
            item.item_attribute2 = item_attribute2;
            item.item_type = item_type;
            item.sku = sku_insert.sku;
            item.barcode_code = barcode_code;
            item.barcode_company = barcode_company;
            item.number = number;
            item.date = date;
            let queueModel = new Item(item);
            await queueModel.save();
            console.log("Insert Ok");
            res.send({
                "item_name": item.item_name,
                "item_type": item.item_type,
                "sku": item.sku,
                "Barcode": item.barcode_company + item.barcode_code,

            });
        }
        catch (e) {
            console.log("---------- please new insert it worng format-------------------");
            console.log(e)
            res.send({ "error": "error" })
        }
    } else {
        console.log("---------- it repeatedly-------------------");
        res.send({
            "error": "repeatedly_in_database"

        });
    }
});

// ---------------------------------------------
//  sort อีกเเบบที่จะทำอนเเรก
// route.post('/sort', async (req, res) => {
//     const {
//         sort_type
//     } = req.body;
//   try {
//     console.log("------------------------------------------------------------------------")
//         if(sort_type == "number"){
//             appointments = await item.find().sort({ number: -1 })
//             console.log("sort by number")
//         }
//         else if(sort_type == "barcode"){
//             appointments = await item.find().sort({ barcode_company: -1,barcode_code: -1 })
//             console.log("sort by barcode")
//         }
//         else if(sort_type == "company"){
//             appointments = await item.find().sort({ item_company: -1 })
//             console.log("sort by company")
//         }
//         else if(sort_type == "sku"){
//             appointments = await item.find().sort({ sku_name: -1,sku_code: -1,sku_attr1: -1,sku_attr2: -1 })
//             console.log("sort by sku")
//         }
//         else if(sort_type == "item_code"){
//             appointments = await item.find().sort({ item_code: -1 })
//             console.log("sort by sku")
//         }
//         else if(sort_type == "item_type"){
//             appointments = await item.find().sort({ item_type: -1 })
//             console.log("sort by item_type")
//         }

//         else if(sort_type == "date"){
//             console.log("sort by item_type")
//             appointments = await item.find().sort({date: -1})
//             console.log("sort by item_type")
//         }
//         res.send(appointments)
//         console.log("sort ok")
//     }
//     catch (e) {
//         console.log("error catch")
//         res.send(e)
//     }
// });
// -------------------------------------------------------------------------


route.get('/sort/', async (req, res) => {

    try {
        console.log("------------------------------------------------------------------------")
        appointments = await item.find()
        console.log("sort by number")
        res.send(appointments)
        console.log("sort ok")
    }
    catch (e) {
        console.log("error catch")
        res.send(e)
    }
});

route.get('/sort/number', async (req, res) => {

    try {
        console.log("------------------------------------------------------------------------")
        appointments = await item.find().sort({ number: -1 })
        console.log("sort by number")
        res.send(appointments)
        console.log("sort ok")
    }
    catch (e) {
        console.log("error catch")
        res.send(e)
    }
});

route.get('/sort/barcode', async (req, res) => {

    try {
        console.log("------------------------------------------------------------------------")
        appointments = await item.find().sort({ barcode_company: -1, barcode_code: -1 })
        console.log("sort by barcode")
        res.send(appointments)
        console.log("sort ok")
    }
    catch (e) {
        console.log("error catch")
        res.send(e)
    }
});


route.get('/sort/sku', async (req, res) => {

    try {
        console.log("------------------------------------------------------------------------")
        appointments = await item.find().sort({ sku: -1})
        console.log("sort by sku")
        res.send(appointments)
        console.log("sort ok")
    }
    catch (e) {
        console.log("error catch")
        res.send(e)
    }
});


route.get('/sort/item_type', async (req, res) => {

    try {
        console.log("------------------------------------------------------------------------")
        appointments = await item.find().sort({ item_type: -1 })
        console.log("sort by item_type")
        res.send(appointments)
        console.log("sort ok")
    }
    catch (e) {
        console.log("error catch")
        res.send(e)
    }
});
route.get('/sort/date', async (req, res) => {

    try {
        console.log("------------------------------------------------------------------------")
        appointments = await item.find().sort({ date: -1 })
        console.log("sort by item_type")
        res.send(appointments)
        console.log("sort ok")
    }
    catch (e) {
        console.log("error catch")
        res.send(e)
    }
});



// -------------------------------------------------------
// เอาไว้ get ค่า ตาม sku ที่ใส่เข้ามออกเเค่บาร์โค้ต
route.get('/barcode/:id', async (req, res) => {
    const _id = req.params.id
    const sku = {
        sku: _id
    }
    console.log(sku)
    try{
    appointment = await Item.find(sku)
    const barcodeall = appointment[0].barcode_company + appointment[0].barcode_code
    res.send({ "barcode_all": barcodeall });
    }catch(e){
        console.log(e)
        res.send("pls new insert sku ")
        
    }
});

// ---------------------------------------------------------------------------------------------------------------------
//  เอาค่าทั้งหมดออกมา 
route.get('/', async (req, res) => {
    try {
        const appointments = await item.find().sort({ number: -1 })
        res.send(appointments)
    }
    catch (e) {
        res.status(500).send(e)
    }
});

// ---------------------------------------------------------------------------------------------------------------------
//  เอาค่าตาม sku ที่ใส่ออกมา
route.get('/:id', async (req, res) => {
    const _id = req.params.id
    try {
        const sku = {
            sku: _id
        }
        console.log(sku)
        appointment = await Item.find(sku)
        res.send(appointment)
    }
    catch (e) {
        console.log("Not have sku")
        res.send(e)
    }
})




// ---------------------------------------------------------------------------------------------------------------------
//  ลบตามรหัส sku 
route.delete('/:id', async (req, res) => {
    const _id = req.params.id
    try {
        const sku = {
            sku: _id
        }
        const appointment = await item.findByIdAndDelete(await Item.find(sku))
        res.send(appointment)
    }
    catch (e) {
        res.send(e)
        console.log("error")
    }
})

// ---------------------------------------------------------------------------------------------------------------------
// เปลี่ยนค่าตามรหัส skuที่เอาออกมา สามารถเปลี่ยนค่าได้ตาม allowedUpdates 
route.patch('/:id', async (req, res) => {
    const updates = Object.keys(req.body)
    const _id = req.params.id
    const allowedUpdates = ['item_name', 'item_attribute1', 'item_attribute2', 'item_type',
                            'barcode_code', 'barcode_company', 'number']
    try {
        const sku = {
            sku: _id
        }
        const myid = await Item.find(sku)
        console.log(sku)
        console.log(myid[0])
        updates.forEach((update) => myid[0][update] = req.body[update])
        await myid[0].save()
        res.send(myid)
        console.log("-------------------update---------------------------")
    }
    catch (e) {
        console.log(e)
        console.log("--------Error dont have sku-----")
    }
})







function makeSku(item_name,item_attribute1,item_type,item_attribute2) {
    //สร้างรหัส sku ขึ้นมา

        a1 = forZero(abbreviate(item_name, {length: 4}),4)
        a2 = forZero(abbreviate(item_attribute1,  {length: 2}),2)
        a3 = forZero(abbreviate(item_attribute2 , {length: 2}),2)
        a4 = forZero(abbreviate(item_type,        {length: 2}),2)
        const sku = a1+a2+a3+a4
    return sku;
}
function forZero(abbreviate,num){
    // เติมรหัส sku
    for(i = 0; i < num-abbreviate.length;){
        abbreviate = "0"+abbreviate
    }
    return  abbreviate
}


module.exports = route;