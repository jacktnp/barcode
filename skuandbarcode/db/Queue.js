const mongoose = require('mongoose');
const moment = require('moment')
const item = new mongoose.Schema({

    item_name: {
        type: String,
        required: true,
    },
    item_attribute1: {
        type: String,
        required: true,
    },
    item_attribute2: {
        type: String,
        required: true,
    },
    item_type: {
        type: String,
        required: true,
    },
    sku:{
        type: String,
        required: true,
        validate(value) {
            if (value.length != 10 ){
                throw new Error('Sku_code 10 String')
            }
        }
    },
    barcode_code: {
        require: true,
        type: String,
        validate(value) {
            if (value.length != 6) {
                throw new Error('barcode_code 6 String')
            }
        }
    },
    barcode_company: {
        type: String,
        require: true,
        validate(value) {
            if (value.length != 5) {
                throw new Error('barcode_company 5 String')
            }
        }
    },
    number: {
        type: Number
    },
    date: {
        type: String,
        require: true,
        // validate(value){
        //     if(!moment(value,"YYYY-MM-DD HH:mm:ss", true)){
        //         throw new Error('pls insert this format ex. 2015-02-13 15:12:39')
        //     }
        // }
    }

}
);




module.exports = Item = mongoose.model('item', item);