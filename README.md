Sku and Barcode
---
ระบบจัดการข้อมูล sku and barcode โดยจะน้ำข้อมูลมาทำ barcode เเละ  ช่วยจัดการ เรื่อง sku โดยจะรับข้อมูลมาหลังจากนั้นจะสามารถนำข้อมูลมาเปลี่ยนเป็น barcode ได้

Sku and Barcode Entity
---
```
{ 
"item_name" : String,
"item_attribute" :String,
"item_type" : "String,
"item_company" : String,
"item_code" : String ,
"sku_nam": String,
"sku_code":String,
"sku_attr1":String,
"sku_attr2":String,
"barcode_code":String,
"barcode_company":String
}
```

Sku and Barcode Service 
---

### Create

##### POST/code
```
{ 
	"item_name" : "Mouse Logitech 270",
	"item_attribute" : "Color: Blue,Batterry : AAA",
	"item_type" : "Computer",
	"item_company" : "Logitech",
	"item_code" : "001" ,
	"sku_nam": "GGEZ",
	"sku_code": "IT",
	"sku_attr1": "12",
	"sku_attr2": "XL",
	"barcode_code":"123456",
	"barcode_company":"134679"
}
```


### Read all
```GET /code```

### Read by ID
```GET /code/{id}```

### Update 
```PATCH /code/{id}```

```
{
อยาก update อันไหนก็ใส่ลงไป
}
```


### Delete
```delete/code/{id}```


### สร้าง Barcode
```get /barcode/:id'```